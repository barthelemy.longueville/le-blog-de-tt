---
title:  "Rally d'Islande"
date:   2022-08-01 08:51:47 +0200
img: "vignette.jpg"
categories: [one, two]
author: "T.T."
---

Ceci est un dessin (toujours à la tablette) du rallye d\'Islande avec une Talbot Samba rallye et un Peugeot 207 !


{{< figure src="RALLYE D_ISLANDE_220728_170518.jpg" title= "Rally d`Islande" >}}