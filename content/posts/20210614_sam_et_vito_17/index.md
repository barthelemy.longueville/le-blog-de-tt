---
title:  "Sam et Vito, Page 17"
date:   2021-06-14 12:51:47 +0200
img: "sam_et_vito_17.jpg"
categories: [one, two]
author: "T.T."
---

Tiens, Vito a découvert une moto, il va pouvoir sortir, prévenir la police et libérer son ami.

{{< figure src="Page_17.jpg" title= "Sam et Vito, Page 17" >}}
