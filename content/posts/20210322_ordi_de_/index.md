---
title:  "Ordi de m.."
date:   2021-03-22 12:51:47 +0200
img: "ordi.jpg"
categories: [one, two]
author: "T.T."
---

Avant d\'allumer votre ordi, surtout quand il est neuf, bien penser à vérifier qu'il est branché !
Surtout si vous êtes de mauvaise humeur.

{{< figure src="Bonus_032.jpg" title= "Ordi de m... 1" >}}

{{< figure src="Bonus_033.jpg" title= "Ordi de m... 2" >}}
