---
title:  "15 - Copie"
date:   2020-12-19 10:51:47 +0200
img: "copie.jpg"
categories: [one, two]
author: "T.T."
---

Je vous donne un conseil, mes amis, quand vous allez dans un musée, tâchez de rien copier du tout !

{{< figure src="15_Page_15.jpg" title="( Les aventures de T.T. Tome 1 Page 15 )" >}}