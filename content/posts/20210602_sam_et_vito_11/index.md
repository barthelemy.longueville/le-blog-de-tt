---
title:  "Sam et Vito, Page 11"
date:   2021-06-02 12:51:47 +0200
img: "page11.jpg"
categories: [one, two]
author: "T.T."
---

Ils rencontrent un mystérieux personnage qui tire presque comme Lucky Luke. Qui est-ce ?

{{< figure src="Page_11.jpg" title= "Page 11" >}}
