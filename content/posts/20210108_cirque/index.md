---
title:  "Quel cirque !"
date:   2021-01-08 12:51:47 +0200
img: "cirque.jpg"
categories: [one, two]
author: "T.T."
---

Si vous conduisez, concentrez vous sur la route, pas sur le paysage !

{{< figure src="24_Page_24.jpg" title="(Quel cirque !)" >}}