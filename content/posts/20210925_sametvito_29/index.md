---
title:  "Sam et Vito, page 29"
date:   2021-09-25 08:51:47 +0200
img: "sv_29.jpg"
categories: [one, two]
author: "T.T."
---

Coincés dans un embouteillage ! Mine de rien pas comme dans les films, les héros dans les courses poursuites sont quand même obligés de ralentir !

{{< figure src="Page_29.jpg" title= "Page 29" >}}
