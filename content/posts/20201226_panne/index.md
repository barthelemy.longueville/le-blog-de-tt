---
title:  "Panne"
date:   2020-12-26 12:51:47 +0200
img: "panne.jpg"
categories: [one, two]
author: "T.T."
---

Si vous avez une voiture et que vous tombez en panne, évitez de vous faire dépanner par une épave sur roues !

{{< figure src="18_Page_18.jpg" title="(Panne ! )" >}}