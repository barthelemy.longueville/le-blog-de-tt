---
title:  "Sam et Vito, Page 18"
date:   2021-06-15 12:51:47 +0200
img: "sam_et_vito_18.jpg"
categories: [one, two]
author: "T.T."
---

Retournons sur le lieu du combat. C\'est au tour de Sam de s\'attaquer à leur ennemi.
On dirait que Sam est KO ! J\'espère que non \...

{{< figure src="Page_18.jpg" title= "Sam et Vito, Page 18" >}}
