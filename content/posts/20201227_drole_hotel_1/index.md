---
title:  "Drôle d'hotel"
date:   2020-12-27 12:51:47 +0200
img: "drolehotel1.jpg"
categories: [one, two]
author: "T.T."
---

Voici la première planche d\'une série de deux planches consacrée à de drôles d\'hôtels où il se passent des choses rigolottes.

{{< figure src="19_Page_19.jpg" title="(Drôle d`hôtel ! )" >}}