---
title:  "Dédicaces attitude"
date:   2021-02-16 12:51:47 +0200
img: "dedicaces.jpg"
categories: [one, two]
author: "T.T."
---

Merci à Boulet pour l\'inspiration, à part à quelques copains ou à la famille, je n\'ai pas encore fait de dédicaces publiques ... mais j\'ai imaginé à quoi cela pourrait ressembler !

{{< figure src="Bonus_019.jpg" title="" >}}
{{< figure src="Bonus_020.jpg" title="" >}}
{{< figure src="Bonus_021.jpg" title="" >}}
{{< figure src="Bonus_022.jpg" title="" >}}
{{< figure src="Bonus_023.jpg" title="" >}}
{{< figure src="Bonus_024.jpg" title="" >}}
{{< figure src="Bonus_025.jpg" title="" >}}
{{< figure src="Bonus_026.jpg" title="" >}}
{{< figure src="Bonus_027.jpg" title="( Dédicaces attitude )" >}}