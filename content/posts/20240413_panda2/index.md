---
title:  "Panda et la pasteque"
date:   2024-04-13 13:59:59 +0200
img: "vignette.jpg"
categories: [one, two]
author: "T.T."
---

Vous aussi vous avez faim ? Une pasteque fera l\'affaire et en plus, elle est rafraichissante !

{{< figure src="2024-04-13_panda-006.jpg" title= "p6" >}}
{{< figure src="2024-04-13_panda-007.jpg" title= "p7" >}}
{{< figure src="2024-04-13_panda-008.jpg" title= "p8" >}}
{{< figure src="2024-04-13_panda-009.jpg" title= "p9" >}}
{{< figure src="2024-04-13_panda-0010.jpg" title= "p10" >}}
{{< figure src="2024-04-13_panda-011.jpg" title= "p11" >}}
{{< figure src="2024-04-13_panda-012.jpg" title= "p12" >}}

