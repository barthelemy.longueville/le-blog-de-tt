---
title:  "Sam et Vito, Page 21"
date:   2021-06-18 12:51:47 +0200
img: "sam_et_vito_21.jpg"
categories: [one, two]
author: "T.T."
---

Un étrange personnage accueille Vito dans un lieu secret qui semble être le futur au présent !

{{< figure src="Page_21.jpg" title= "Sam et Vito, Page 21" >}}
