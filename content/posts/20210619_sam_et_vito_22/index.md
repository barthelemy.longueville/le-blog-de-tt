---
title:  "Sam et Vito, Page 22"
date:   2021-06-19 12:51:47 +0200
img: "sam_et_vito_22.jpg"
categories: [one, two]
author: "T.T."
---

Le personnage mystérieux dévoile son visage et lui demande d\'aller chercher son frère.

{{< figure src="Page_22.jpg" title= "Sam et Vito, Page 22" >}}
