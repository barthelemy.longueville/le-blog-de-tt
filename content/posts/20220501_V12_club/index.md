---
title:  "V12 Club, les personnages"
date:   2022-05-01 08:51:47 +0200
img: "vignette.jpg"
categories: [one, two]
author: "T.T."
---

Voici les personnages de V12 club. Il sont au nombre de cinq et ont chacun un surnom et des nationalités différentes et des jeux de mots dans leurs noms. 

A vous de les trouver !

{{< figure src="01_page_01.jpg" title= "V12 Club - Personnages" >}}
