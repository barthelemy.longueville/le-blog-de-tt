---
title:  "Rivère"
date:   2022-07-30 08:51:47 +0200
img: "vignette.jpg"
categories: [one, two]
author: "T.T."
---

Après l\'aquarelle, j\'explore le dessin à la tablette, pour l\'instant le matériel n\'est pas très évolué (samsung note) mais le résultat est quand même bien !

Voici un dessin de rivière que j\'ai inventée.


{{< figure src="RIVIERE_220724_160836.jpg" title= "Rivère" >}}