---
title:  "Sam et Vito - Page 40"
date:   2022-04-01 08:51:47 +0200
img: "vignette.jpg"
categories: [one, two]
author: "T.T."
---

Voilà la suite de l\'aventure de Sam et Vito que vous attendez peut-être depuis quelques mois.

Juste pour information, la marque des véhicule est Goodcar. Je l\'ai inventée à partir du nom de leur créateur : Goodfehdoom.


{{< figure src="Page_40.jpg" title= "Sam et Vito page 40" >}}
