---
title:  "Sam et Vito, page 34"
date:   2021-11-01 08:51:47 +0200
img: "sv_34.jpg"
categories: [one, two]
author: "T.T."
---

La police a arrêté Götferdöm. Sam et Vito restent sur le terrain afin d\'y retrouver quelque chose sans but précis.

{{< figure src="Page_34.jpg" title= "Page 34" >}}
