---
title:  "Making of filmo #2"
date:   2023-07-12 13:59:59 +0200
img: "vignette.jpg"
categories: [one, two]
author: "T.T."
---

Je vous presente (retooour a l\'azerty ) la suite du making of de l\'Atelier Filmo (post Making of) avec le crayonne (pronnocer crayoner) et l\'encrage, fait sur table lumineuse.
                       

{{< figure src="20_crayonné_final.jpg" title= "crayonne" >}}
{{< figure src="30_repassage_essai.jpg" title= "encrage essais" >}}
{{< figure src="31_repassage_final.jpg" title= "encrage" >}}
