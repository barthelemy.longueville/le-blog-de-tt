---
title:  "Aquarelle (encore une fleur)"
date:   2022-09-23 08:51:47 +0200
img: "vignette.jpg"
categories: [one, two]
author: "T.T."
---

J\'ai réalisé cette aquarelle grâce à un mélande de réalité et de fiction: la fleur est réelle mais j\'ai imaginé le fond et le pot.

{{< figure src="Bonus_044_hardelot.jpg" title= "Aquarelle, fleur, Hardelot" >}}
