---
title:  "Sam et Vito - Page 41"
date:   2022-04-03 08:51:47 +0200
img: "vignette.jpg"
categories: [one, two]
author: "T.T."
---

Sur cette page, il y a une petite erreur que j\'ai découverte en me relisant et me renseignant :
Les courses annoncées, ce n\'était pas un grand prix mais une compétition, car le grand-prix n\'est qu\'une seule course de formule 1.


{{< figure src="Page_41.jpg" title= "Sam et Vito page 41" >}}
