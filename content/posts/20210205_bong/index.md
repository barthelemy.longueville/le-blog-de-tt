---
title:  "Bong !"
date:   2021-02-05 12:51:47 +0200
img: "bong.jpg"
categories: [one, two]
author: "T.T."
---

Cette est tirée d\'une histoire vraie. Cela m\'est arrivé en Allemagne dans une énorme descente de VTT. D\'ailleurs j\'en ai encore des cicatrices !

{{< figure src="35_Page_35.jpg" title="( Bong ! )" >}}