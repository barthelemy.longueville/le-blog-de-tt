---
title:  "02 - Naissance d'une idée"
date:   2020-11-20 10:51:47 +0530
img: "naissance.png"
categories: [one, two]
author: "T.T."
---

Cette page est spéciale car ce n\'est pas un gag, c\'est juste une explication modifiée (un peu exagérée) de la naissance de cette BD.


{{< figure src="02_Page_02.png" title="( Les aventures de T.T. Tome 1, Page 2 )" >}}
