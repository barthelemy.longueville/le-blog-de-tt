---
title:  "Brik à Brak Tome 1 - Couverture - Méli Mélo de BD"
date:   2021-04-11 12:51:47 +0200
img: "couv.jpg"
categories: [one, two]
author: "T.T."
---

Je démarre une autre série en même temps que Sam et Vito (intitulée Brik à Brak). 

J\'espère que cela va vous plaire. C\'est un mélange de tous mes personnages et de plein de surprises.


{{< figure src="Brik-a-Brak_00_couverture.jpg" title= "Brik à Brak Tome 1 - Couverture - Méli Mélo de BD" >}}
