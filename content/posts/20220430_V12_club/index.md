---
title:  "V12 Club Couverture"
date:   2022-04-30 08:51:47 +0200
img: "vignette.jpg"
categories: [one, two]
author: "T.T."
---


Voici l\'héritier de \'\'Les bagnoles\'\' ( si vous ne savez pas de quoi je parle, suivez ce
 [lien]({{< ref "/posts/20210313_pub-0002">}}) ) !

 Cette fois-ci la BD est en couleur avec des personnes (hauts en couleurs également) à découvrir dans les articles suivants.


{{< figure src="00_couv.jpg" title= "V12 Club - Couverture" >}}
