---
title:  "Sam et Vito, Page 9"
date:   2021-04-10 12:51:47 +0200
img: "montre.jpg"
categories: [one, two]
author: "T.T."
---

Heureusement l'ennemi est K.O., la cellule c\'est une chose, mais il reste à s\'évader du QG des bandits !
Ils vont devoir se creuser les méninges.

{{< figure src="Page_09_edit.jpg" title= "Page 9" >}}
