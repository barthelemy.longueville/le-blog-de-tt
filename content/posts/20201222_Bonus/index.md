---
title:  "Bonus - Bouboulman vs. Traitman"
date:   2020-12-22 12:51:47 +0200
img: "bouboulman.jpg"
categories: [one, two]
author: "T.T."
---

Chic ! Enfin une B.D complète en bonus avec deux nouveaux personnages, Traitman et Bouboulman !

{{< figure src="Bonus_010.jpg" title="( Bonus - Bouboulman vs. Traitman 1 )" >}}

{{< figure src="Bonus_011.jpg" title="( Bonus - Bouboulman vs. Traitman 2 )" >}}

{{< figure src="Bonus_012.jpg" title="( Bonus - Bouboulman vs. Traitman 3 )" >}}