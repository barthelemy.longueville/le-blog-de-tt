---
title:  "Making of Filmo #2"
date:   2023-07-15 13:59:59 +0200
img: "vignette.jpg"
categories: [one, two]
author: "T.T."
---
La suite du making of avec le crayonne et l\'encrage. 


{{< figure src="20_crayonné.jpg" title= "crayonne" >}}
{{< figure src="21_crayonné.jpg" title= "crayonne 2" >}}
{{< figure src="40_repassage.jpg" title= "encrage" >}}
{{< figure src="41_repassage.jpg" title= "encrage2" >}}
{{< figure src="50_essai_couverture.jpg" title= "couv essai" >}}