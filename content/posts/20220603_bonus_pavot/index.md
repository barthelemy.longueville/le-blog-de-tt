---
title:  "Bonus Pavot"
date:   2022-06-03 08:51:47 +0200
img: "vignette.jpg"
categories: [one, two]
author: "T.T."
---

Ma première vraie aquarelle ! Voici un pavot que j\'ai dessiné en Normandie.


{{< figure src="Bonus_042.jpg" title= "Pavot normand" >}}