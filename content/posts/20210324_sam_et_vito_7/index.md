---
title:  "Sam et Vito, Page 7"
date:   2021-03-24 12:51:47 +0200
img: "sam_7.jpg"
categories: [one, two]
author: "T.T."
---

Enfin les retrouvailles, c\'est dans une prison sombre, malheureusement, que Sam retrouve son ami Vito !
Comme la plupart des héros que vous connaissez, ils décident de s'évader. Comment vont-il s'y prendre ?

{{< figure src="Page_07.jpg" title= "Retrouvailles" >}}
