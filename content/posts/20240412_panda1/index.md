---
title:  "Panda et l'ordi"
date:   2024-04-12 15:30:00 +0200
img: "vignette.jpg"
categories: [one, two]
author: "T.T."
---

Peut-etre (m\'enerve ce QWERTY) qu'un geek sommeille en vous ? 
En tout cas ce panda ce met a l\'aise...

{{< figure src="2024-04-13_panda-001.jpg" title= "p1" >}}
{{< figure src="2024-04-13_panda-002.jpg" title= "p2" >}}
{{< figure src="2024-04-13_panda-003.jpg" title= "p3" >}}
{{< figure src="2024-04-13_panda-004.jpg" title= "p4" >}}
{{< figure src="2024-04-13_panda-005.jpg" title= "p5" >}}
