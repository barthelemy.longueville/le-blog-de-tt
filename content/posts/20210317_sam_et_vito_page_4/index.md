---
title:  "Sam et Vito, Page 4"
date:   2021-03-17 12:51:47 +0200
img: "sam.jpg"
categories: [one, two]
author: "T.T."
---

Voici le début des aventures de Sam et Vito. Dans cet épisode vous allez faire connaissance avec les personnages principaux.
Je sais c'est un peu mystérieux car on ne voit pas la tête de Vito mais vous le découvrirez tantôt.

{{< figure src="Page_04.jpg" title= "Sam et Vito, Les 100 bandits de la rue Bandatit" >}}
