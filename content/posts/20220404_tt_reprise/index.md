---
title:  "T.T Reprise"
date:   2022-04-04 08:51:47 +0200
img: "vignette.jpg"
categories: [one, two]
author: "T.T."
---

Si vous vous souvenez de la page 23 du tome 1 de T.T, et bien vous verrez que cette page en est une reprise en couleurs.
Sinon, allez-voir [ici]({{< ref "/posts/20210107_danger/index.md" >}}).


{{< figure src="extra_tome1_page23.jpg" title= "T.T reprise" >}}
