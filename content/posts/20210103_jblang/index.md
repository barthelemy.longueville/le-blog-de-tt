---
title:  "JBLANG !"
date:   2021-01-03 12:51:47 +0200
img: "fini.jpg"
categories: [one, two]
author: "T.T."
---

Quand vous triez et vous rangez vos affaires, évitez de les jetter en vrac ou de les jetter sur une table derrière vous.

{{< figure src="21_Page_21.jpg" title="(Drôle d`hôtel ! )" >}}