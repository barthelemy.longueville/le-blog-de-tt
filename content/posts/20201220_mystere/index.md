---
title:  "16 - Mystère"
date:   2020-12-20 12:51:47 +0200
img: "mystere.jpg"
categories: [one, two]
author: "T.T."
---

Attention, si vous avez une lampe qui ne fonctionne pas quand une autre est en marche, essayez d'éteindre celle-ci.

{{< figure src="16_Page_16.jpg" title="( Les aventures de T.T. Tome 1 Page 16 )" >}}