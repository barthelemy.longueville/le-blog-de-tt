---
title:  "Shnak et companie - Page 1"
date:   2022-10-20 08:51:47 +0200
img: "vignette.jpg"
categories: [one, two]
author: "T.T. & Loulou"
---


Voici une nouvelle BD que je n\'ai pas faite seul. Je l\'ai crée avec mon ami Loulou (son pseudo) dans un délire.

C\'est l\'histoire de deux aventuriers à la recherche d\'un crocodile géant appelé Shnak vivant dans la jungle Otokzztanienne !




{{< figure src="Shnak_et_co_page_01.jpg" title= "Shnak et companie - Page 1" >}}
