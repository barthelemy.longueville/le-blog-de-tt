---
title:  "Petites aquarelles"
date:   2023-02-12 13:59:59 +0200
img: "vignette.jpg"
categories: [one, two]
author: "T.T."
---

Bonne annee !(Desole j\'ai un cavier QWERTY)
J\'ai eu l\'idee de faire des petites aquarelles pour m\'amuser. 
Pas de chichis:du papier classique et un style BD!


{{< figure src="foot.jpg" title= "Foot" >}}

{{< figure src="animaux.jpg" title= "animaux" >}}

{{< figure src="chat.jpg" title= "chat" >}}

{{< figure src="grenouille.jpg" title= "grenouille" >}}

{{< figure src="lemmings.jpg" title= "lemmings" >}}

{{< figure src="monstres.jpg" title= "monstre" >}}

{{< figure src="montagne.jpg" title= "montagnes" >}}

{{< figure src="pingouin.jpg" title= "pingouin" >}}

{{< figure src="tt.jpg" title= "tt" >}}

{{< figure src="4tt.jpg" title= "TT" >}}