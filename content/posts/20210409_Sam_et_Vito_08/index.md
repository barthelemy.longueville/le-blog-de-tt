---
title:  "Sam et Vito, Page 8"
date:   2021-04-09 12:51:47 +0200
img: "epinards.jpg"
categories: [one, two]
author: "T.T."
---

Sam a un plan d\'évasion. Mais contrairement à ce qu\'ils pensaient, ils se retrouvent face à colosse professionnel !
Feront-ils le poids ?

{{< figure src="Page_08.jpg" title= "Page 8 - Le colosse" >}}
