---
title:  "NGCX13 - couverture"
date:   2022-10-21 08:51:47 +0200
img: "vignette.jpg"
categories: [one, two]
author: "T.T., Loulou et Riri"
---

Encore une nouveau et une collaboration, cette BD, intitulée NGCX13 a été réalisée avec mes amis Loulou et Riri.

Dans un futur proche, cette histoire d\'exploration spatiale et de robots vous conduira aux fins fonds de l'univers grâce à nouveau combustible :  le plasma.




{{< figure src="NGCX13_Couverture.jpg" title= "NGCX13 - La couverture" >}}
