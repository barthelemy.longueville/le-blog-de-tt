---
title:  "Sam et Vito, Les 100 bandits de la rue Bandatit"
date:   2021-03-15 12:51:47 +0200
img: "sam_et_vito.jpg"
categories: [one, two]
author: "T.T."
---

Youpi ! Bon, j\'ai du changer un peu le programme, en attendant le tome 2 des aventures de T.T, je sors un nouvel album, **Sam et Vito, Les 100 bandits de la rue Bandatit**. 

Retrouvez les aventures de mes deux nouveaux héros, garanti 100% suspens ! 


{{< figure src="00_Couverture.jpg" title= "Sam et Vito, Les 100 bandits de la rue Bandatit" >}}
