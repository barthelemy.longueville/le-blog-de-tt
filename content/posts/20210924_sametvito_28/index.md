---
title:  "Sam et Vito, page 28"
date:   2021-09-24 08:51:47 +0200
img: "sv_28.jpg"
categories: [one, two]
author: "T.T."
---

Oula, deuxième crevaison ! Mais il y a toujours un moyen de dépannage \...
Dommage pour Sam et Vito ! 

Mais où est passé le collègue de Gödferdöm ?

{{< figure src="Page_28.jpg" title= "Page 28" >}}
