---
title:  "Bye bye !"
date:   2021-02-09 12:51:47 +0200
img: "byebye.jpg"
categories: [one, two]
author: "T.T."
---

Au revoir, à la prochaine !

{{< figure src="39_Page_39.jpg" title="( Bye bye !)" >}}