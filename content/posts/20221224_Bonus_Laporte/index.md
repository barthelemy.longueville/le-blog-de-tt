---
title:  "Joyeux Noël! On pense à Laporte"
date:   2022-12-24 23:59:59 +0200
img: "vignette.jpg"
categories: [one, two]
author: "T.T."
---

J\'étais sur mon bureau et j\'ai tout d\'un coup pensé à Laporte, et,HOP!, j\'ai eu l\'idée de cette BD muette et simple.

Je sais que certains de mes lecteurs connaissent ce hameau à coté de Lapleau, en Corrèze.

ET JOYEUX NOEL!!



{{< figure src="Laporte_1.jpg" title= "l1" >}}

{{< figure src="Laporte_2.jpg" title= "l2" >}}

{{< figure src="Laporte_3.jpg" title= "l3" >}}

{{< figure src="Laporte_4.jpg" title= "l4" >}}
