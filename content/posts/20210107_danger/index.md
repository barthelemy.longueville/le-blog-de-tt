---
title:  "Danger"
date:   2021-01-07 12:51:47 +0200
img: "atelier.jpg"
categories: [one, two]
author: "T.T."
---

Si vous visitez un laboratoire, vérifiez __vraiment__ si vous êtes en sécurité. Même deux fois plutôt qu\'une !

{{< figure src="23_Page_23.jpg" title="(Danger )" >}}