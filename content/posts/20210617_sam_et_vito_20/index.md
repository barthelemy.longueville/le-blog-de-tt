---
title:  "Sam et Vito, Page 20"
date:   2021-06-17 12:51:47 +0200
img: "sam_et_vito_20.jpg"
categories: [one, two]
author: "T.T."
---

Aïe ! Vito s\' est cogné contre un mur. Il est mystérieusement aspiré par une bouche d\'aération. Mais que va-t\'il se passer ?

{{< figure src="Page_20.jpg" title= "Sam et Vito, Page 20" >}}
