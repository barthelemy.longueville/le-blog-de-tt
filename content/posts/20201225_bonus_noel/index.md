---
title:  "Bonus - Réveillon de Noël"
date:   2020-12-25 12:51:47 +0200
img: "bonus.jpg"
categories: [one, two]
author: "T.T."
---

Comme vous avez l\'air impatient, je vous offre pour ce Noël les pages 9 et 10 des aventures de T.T. tome 2, en avant première !

{{< figure src="Bonus_013.jpg" title="( Bonus de Noël )" >}}