---
title:  "Bonus voitures #2"
date:   2022-04-29 08:51:47 +0200
img: "vignette.jpg"
categories: [one, two]
author: "T.T."
---

Vous souvenez-vous du bonus voiture de décembre 2020 ? 
C\'est [ici au cas où]({{<ref "/posts/20201216_bonus" >}}) ? 

Dans ce post retrouvez 14 voitures françaises cultes sous forme de croquis.


{{< figure src="Bonus_039.jpg" title= "14 voitures cultes, partie 1" >}}



{{< figure src="Bonus_040.jpg" title= "14 voitures cultes, partie 2" >}}
