---
layout: page
title: A propos de l'auteur
permalink: /a-propos/
---

Bonjour chers lectrices, chers lecteurs, je suis T.T., en vrai je m'appelle Théophile.

Sur ce blog vous pourrez lire les planches de ma première BD, "Les aventures de T.T., Tome 1 : Chic, une idée". Cette BD est née à peu près comme dans la [première histoire]({{< ref "/posts/20201120_Naissance_d_une_idee" >}}), mais en réalité je ne me suis pas cogné sur le haut de la porte.

Moi j'ai huit ans mais mon personnage en a vingt de plus. J'espère que vous aimerez ses aventures.

{{< figure src="/images/theophile.jpg" title="( T.T. )" >}}

Maintenant j'ai dix ans et j'ai déjà fait beaucoup de posts sur ce blog, voici une photo plus récente sur laquelle on peut appercevoir mon nouveau bureau.

{{< figure src="/images/Theophile_2022.jpg" title="( T.T. 2022)" >}}
